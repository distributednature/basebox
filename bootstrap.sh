#!/bin/sh

# box provisioned with:
apt-get update -y
apt-get install nginx python-software-properties -y
add-apt-repository -y ppa:chris-lea/node.js
apt-get update -y
apt-get install nodejs -y
npm install forever -g

dpkg -i /vagrant/chefdk_0.6.2-1_amd64.deb
